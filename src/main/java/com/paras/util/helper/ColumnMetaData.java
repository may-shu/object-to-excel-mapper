package com.paras.util.helper;

import java.lang.reflect.Method;

public class ColumnMetaData implements Comparable<ColumnMetaData>{

	private int at;
	private String heading;
	private Method method;
	private String type;
	
	public ColumnMetaData( int at, String heading, String type, Method method ) {
		this.at = at;
		this.heading = heading;
		this.method = method;
		this.type = type;
	}
	
	public int getAt() {
		return at;
	}
	public void setAt(int at) {
		this.at = at;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}
	public String getType() {
		return type;
	}
	public void setType( String type ) {
		this.type = type;
	}
	
	public int compareTo( ColumnMetaData meta ) {
		if( at < meta.getAt() ) {
			return -1;
		} else if( at == meta.getAt() ) {
			return 0;
		}
		
		return 1;
	}
	
	
}
