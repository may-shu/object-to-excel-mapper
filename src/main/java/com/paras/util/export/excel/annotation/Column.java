package com.paras.util.export.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation that will describe field to column mapping.
 * It can later be used in translating object field into a excel column.
 * @author Paras.
 *
 */
@Target( ElementType.FIELD )
@Retention( RetentionPolicy.RUNTIME )
public @interface Column {
	
	public static final String STRING = "String";
	public static final String BOOLEAN = "Boolean";
	public static final String INTEGER = "INTEGER";
	
	/**
	 * Order At which it needs to appear.
	 */
	int at() default 0;
	
	/**
	 * Name of column to which this field will be mapped.
	 */
	String name() default "";
	
	/**
	 * Type of field.
	 */
	String type() default STRING;
}
