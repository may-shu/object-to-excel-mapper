package com.paras.util.export.excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.paras.util.export.excel.annotation.Column;
import com.paras.util.helper.ColumnMetaData;

/**
 * Exporter to export content into excel.
 * @author gaurav
 *
 */
public class Exporter<T> {

	private static final int FIRST = 0;
	private static final int SECOND = 1;

	private String file;
	private Class<T> clazz;
	private List<T> list;

	private static final Logger LOGGER = Logger.getLogger( Exporter.class );

	public Exporter( String file, Class<T> clazz ) {
		this.file = file;
		this.clazz = clazz;
	}

	public Exporter<T> using( List<T> list ) {
		this.list = list;
		return this;
	}

	public Workbook export() {
		LOGGER.info( "In Exporter | Starting Execution of export " );

		Workbook book = null;


		List<ColumnMetaData> metaData = getList( clazz );
		book = new XSSFWorkbook();

		try {
			Sheet sheet = book.createSheet();

			/**
			 * Writing headers.
			 */		
			Row heading = sheet.createRow( FIRST );
			int currColumn = FIRST;
			for( ColumnMetaData data : metaData ) {

				Cell cell = heading.createCell( currColumn++ );
				cell.setCellValue( data.getHeading() );

			}

			int currRow = SECOND;
			for( T obj : list ) {
				currColumn = FIRST;
				Row row = sheet.createRow( currRow++ );

				for( ColumnMetaData data : metaData ) {
					Cell cell = row.createCell( currColumn++ );

					String type = data.getType();
					Method method = data.getMethod();

					if( Column.STRING.equals( type )) {					
						cell.setCellValue( ( String )method.invoke( obj ));

					} else if( Column.INTEGER.equals( type )) {
						cell.setCellValue( ( Integer ) method.invoke( obj ));
					}
				}
			}

			FileOutputStream out;

			out = new FileOutputStream( 
					new File( file ));
			book.write(out);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		LOGGER.info( "In Exporter | Finished Execution of export " );
		return book;
	}

	private List<ColumnMetaData> getList( Class<T> clazz ) {

		List<ColumnMetaData> metas = new ArrayList<ColumnMetaData>();
		Field[] fields = clazz.getDeclaredFields();

		try{
			for( Field field : fields ) {

				if( field.isAnnotationPresent( Column.class )) {

					Column column = ( Column ) field.getAnnotation( Column.class );
					int at = column.at();
					String heading = column.name();
					String type = column.type();

					Method method = getGetter( field.getName(), type );

					ColumnMetaData meta = new ColumnMetaData( at, heading, type, method );
					metas.add( meta );
				}

			}
		} catch (IllegalArgumentException e) {

			e.printStackTrace();
		}

		Collections.sort( metas );

		return metas;

	}

	private Method getGetter( String field, String type ) {
		Method method = null;
		String getter = getGetterName( field, type );

		try{
			method = clazz.getDeclaredMethod( getter );
		} catch( NoSuchMethodException ex ) {
			ex.printStackTrace();

		} catch( SecurityException ex ){ 
			ex.printStackTrace();

		}

		return method;

	}

	private String getGetterName( String field, String type ) {
		if( Column.BOOLEAN.equals( type )) {
			return "is" + toFirstLetterCaptial( field );
		}

		return "get" + toFirstLetterCaptial( field );

	}

	private String toFirstLetterCaptial( String name ) {
		return ( "" + name.charAt(0)).toUpperCase() + name.substring( 1 );
	}
}
